"""Flask service."""
import uuid
from flask import (
    Flask,
    request,
    jsonify,
    abort
    # Response
)
from flasgger import Swagger
from werkzeug.exceptions import BadRequest

from .utils.api_utils import (
    check_url,
    check_email
)
from .utils.misc_utils import (
    get_colored_logger,
    get_db,
    read_config
)

CONFIG = read_config('config.json')['api']
DB_CONFIG = read_config('config.json')['db']

LOG = get_colored_logger('API', CONFIG['log_level'])
get_colored_logger('werkzeug', CONFIG['werkzeug_log_level'])

APP = Flask(__name__)
Swagger(APP)


def register(url: str, email: str = None) -> str:
    """Register request.

    Args:
        url (str): target file url
        email (str, optional): email to sent result to

    Returns:
        uuid.UUID: request ID

    """
    _id = str(uuid.uuid1())

    record = {
        "id": _id,
        "url": url,
        "email": email
    }

    send_record(record)

    return _id


def send_record(record: dict):
    """Send record to queue."""
    import datetime

    db = get_db(config=DB_CONFIG)

    query = {
        "id": record["id"],
        "url": record["url"],
        "email": record["email"],
        "md5": None,
        "added_on": datetime.datetime.utcnow(),
        "status": "pending",
        "error": None
    }

    if query['email'] is not None:
        query['sent'] = "pending"

    try:
        db.queries.insert_one(query)
    except Exception as error:
        LOG.error("Insertion failed: %s", error)
        raise error


@APP.route('/submit', methods=['POST'])
def submit():
    """
    Request file MD5 by URL.

    ---
    tags:
        - submit
    parameters:
        - name: url
          type: string
          in: formData
          required: true
          description: File url
          example: http://example.com
        - name: email
          type: string
          in: formData
          required: false
          description: Email to send results to
          example: example@example.com
    responses:
        400:
            description: Invalid url or email
        500:
            description: Something happened
        201:
            description: Request successfully registered
            schema:
                type: object
                properties:
                    id:
                        type: string

    """
    form = dict(request.form)
    if not form.get('url', False):
        raise BadRequest('You must provide url')

    url = form['url']

    if not check_url(url):
        raise BadRequest('URL is not valid')

    email = form.get('email', None)

    if email is not None:
        if not check_email(email):
            raise BadRequest('Email is not valid')

    try:
        _id = register(url=url, email=email)
        return jsonify({'id': _id}), 201
    except Exception as error:
        LOG.error(str(error).capitalize())
        abort(500)


def get_task(_id: str) -> dict:
    """Pull task from db by ID."""
    db = get_db(config=DB_CONFIG)

    query = {
        "id": _id
    }

    result = db.queries.find_one(query)

    if not result:
        result = {'status': 'not_found'}

    LOG.warning(result)

    return result


def form_response(task: dict) -> dict:
    """Form response for successfull task."""
    response = {
        "md5": task["md5"],
        "status": "done",
        "url": task["url"]
    }
    return response


@APP.route('/check', methods=['GET'])
def check():
    """
    Check status of the task.

    ---
    tags:
        - check
    parameters:
        - name: id
          type: string
          in: url
          required: true
          description: Task ID
          example: a95cff4e-f66a-11e9-87a2-79da97f8545f
    responses:
        400:
            description: Task ID is not provided
        500:
            description: Task has failed
        200:
            description: Request successfully registered
            schema:
                type: object
                properties:
                    id:
                        type: string
        404:
            description: Could not find task with provided ID
        202:
            description: Task processing is in progress

    """
    _id = dict(request.args).get('id')
    if _id is None:
        raise BadRequest('You must provide id')

    task = get_task(_id)

    if task['status'] == "done":
        response = form_response(task)
        return jsonify(response), 200
    elif task['status'] in ["pending", "in_progress"]:
        response = {
            "status": "running"
        }
        return jsonify(response), 202
    elif task['status'] == "not_found":
        response = {
            "status": "not found"
        }
        return jsonify(response), 404
    elif task['status'] == "failed":
        response = {
            "status": "failed",
            "description": f"{task['error']}"
        }
        return jsonify(response), 500
    else:
        abort(500)


def run():
    """Run Flask server."""
    APP.run(host=CONFIG['host'], port=CONFIG['port'], threaded=CONFIG['threaded'], debug=CONFIG['debug_mode'])


if __name__ == '__main__':
    run()
