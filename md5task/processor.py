"""Process tasks."""
import time
import multiprocessing
from multiprocessing import Queue

import pymongo

from .utils.processor_utils import calculate_md5
from .utils.misc_utils import (
    get_db,
    read_config,
    get_colored_logger
)


class Processor():
    """Processes tasks."""

    def __init__(self, config_path: str):
        """Processor constructor.

        Args:
            config_path (str): path of config json

        """
        self._config = read_config(config_path)['processor']
        self._db_config = read_config(config_path)['db']

        self._logger = get_colored_logger('Processor', self._config['log_level'])

        self._processing_queue: multiprocessing.Queue = Queue()

        self._processing_workers: list = []
        self._queue_worker_process = None

    def run(self):
        """Run processing pipeline."""
        self._queue_worker_process = multiprocessing.Process(
            target=self._queue_worker,
            args=(self._processing_queue,)
        )
        self._queue_worker_process.start()

        for wid in range(self._config['processing_workers']):
            process = multiprocessing.Process(
                target=self._processor_worker,
                args=(wid, self._processing_queue)
            )
            process.start()
            self._processing_workers.append(process)

        self._logger.info('All processing workers has started')

    def _queue_worker(self, queue: multiprocessing.Queue):
        logger = get_colored_logger('Processor.queue_worker', self._config['log_level'])

        db = get_db(config=self._db_config)

        while True:
            try:
                results = list(
                    db.queries.find(
                        {
                            "status": "pending"
                        }
                    ).sort("added_on", pymongo.ASCENDING)
                )

                # results = []

                if not results:
                    logger.debug("No new jobs found, sleeping")
                    time.sleep(1)
                    continue

                ids = [result['_id'] for result in results]

                # logger.warning(ids)

                db.queries.update_many(
                    {"_id": {"$in": ids}},
                    {"$set": {"status": "in_progress"}}
                )

                logger.debug("%d new jobs found", len(results))
                for result in results:
                    queue.put(result)
            except Exception as error:
                logger.error('Error while fetching new jobs: %s', error)
                raise

    def _processor_worker(
            self,
            _id: int,
            queue: multiprocessing.Queue
    ):
        logger = get_colored_logger(f'Processor.proc_worker-{_id}', self._config['log_level'])

        db = get_db(config=self._db_config)

        while True:
            task = queue.get()

            try:
                md5 = calculate_md5(url=task['url'])
            except Exception as error:
                logger.warning('Could calculate md5 for a file %s: %s', task['url'], error)
                db.queries.update_one(
                    {'_id': task['_id']},
                    {'$set': {'status': 'failed', 'error': str(error)}}
                )
                continue

            task['md5'] = md5

            db.queries.update_one(
                {'_id': task['_id']},
                {
                    '$set': {
                        'status': 'done',
                        'md5': task['md5']
                    }
                }
            )

            logger.debug("Processed task %s successfully", task['id'])


def run():
    """Run MD5 processor."""
    proc = Processor('config.json')
    proc.run()


if __name__ == '__main__':
    run()
