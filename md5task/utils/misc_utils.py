"""Additional tools."""

import logging
import coloredlogs
import pymongo


def get_colored_logger(name: str, level: str = "INFO") -> logging.Logger:
    """Return a logger with a default ColoredFormatter.

    Levels:
    NOTSET->DEBUG->INFO->WARNING->ERROR->CRITICAL

    Args:
        name (str): Logger name
        level (Optional[str], optional): logging level, defaults to INFO

    Returns:
        logging.Logger: Description

    """
    logger = logging.getLogger(name)
    level = level.upper()

    if level == "INFO":
        logger.setLevel(logging.INFO)
    elif level == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif level == "WARNING":
        logger.setLevel(logging.WARNING)
    elif level == "ERROR":
        logger.setLevel(logging.ERROR)
    elif level == "CRITICAL":
        logger.setLevel(logging.CRITICAL)
    else:
        raise Exception("No such logging level")

    coloredlogs.install(level=level)

    return logger


def get_db(config: dict) -> pymongo.collection.Collection:
    """Get MongoDB client.

    Args:
        config (dict): host, port, name

    Returns:
        pymongo.MongoClient: database client

    """
    from pymongo import MongoClient

    port = config["port"]
    client = MongoClient(config["host"], port)
    database = client[config["db_name"]]

    return database


def read_config(json_path: str) -> dict:
    """Load config from json.

    Args:
        json_path (str): Path to JSON file

    Returns:
        dict: Converted dictionary

    Raises:
        Exception: If there is a problem with the file

    """
    from json import loads

    try:
        with open(json_path) as raw:
            jdict = loads(raw.read())
    except Exception as error:
        raise Exception("Problem with json file {}: {}".format(json_path, error))

    return jdict
