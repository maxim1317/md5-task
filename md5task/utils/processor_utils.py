"""Additional tools for processor."""
import hashlib
import requests


def calculate_md5(url: str) -> str:
    """Calculate MD5 hash of remote file."""
    md5_hash = hashlib.md5()

    try:
        request = requests.get(url)
        for data in request.iter_content(8192):
            md5_hash.update(data)
    except requests.ConnectionError:
        msg = f'Error connecting to {url}'
        raise requests.ConnectionError(msg)

    return md5_hash.hexdigest()
