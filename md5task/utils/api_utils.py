"""Additional tools."""


def check_email(email: str) -> bool:
    """Check email validity using simple regex."""
    import re

    email_regex = r'[^@]+@[^@]+\.[^@]+'

    return re.fullmatch(email_regex, email) is not None


def check_url(url: str) -> bool:
    """Check url validity."""
    from urllib.parse import urlparse

    parse_result = urlparse(url=url)

    result = all([parse_result.scheme, parse_result.netloc])

    return result
