"""Send results via SMTP."""
import time
import smtplib
import multiprocessing
from multiprocessing import Queue

import pymongo

from .utils.misc_utils import (
    get_db,
    read_config,
    get_colored_logger
)


class Sender():
    """Processes tasks."""

    def __init__(self, config_path: str):
        """Sender constructor.

        Args:
            config_path (str): path of config json

        """
        self._config = read_config(config_path)['mail']
        self._db_config = read_config(config_path)['db']

        self._logger = get_colored_logger('Sender', self._config['log_level'])

        self._mailing_queue: multiprocessing.Queue = Queue()

        self._mailing_workers: list = []
        self._queue_worker_process = None

    def run(self):
        """Run processing pipeline."""
        self._queue_worker_process = multiprocessing.Process(
            target=self._queue_worker,
            args=(self._mailing_queue,)
        )
        self._queue_worker_process.start()

        for wid in range(self._config['mailing_workers']):
            process = multiprocessing.Process(
                target=self._mailing_worker,
                args=(wid, self._mailing_queue)
            )
            process.start()
            self._mailing_workers.append(process)

        self._logger.info('All processing workers has started')

    def _queue_worker(self, queue: multiprocessing.Queue):
        logger = get_colored_logger('Sender.queue_worker', self._config['log_level'])

        db = get_db(config=self._db_config)

        while True:
            try:
                results = list(
                    db.queries.find(
                        {
                            "status": "done",
                            "sent": "pending"
                        }
                    ).sort("added_on", pymongo.ASCENDING)
                )

                ids = [result['_id'] for result in results]

                # logger.warning(ids)

                db.queries.update_many(
                    {"_id": {"$in": ids}},
                    {"$set": {"sent": "in_progress"}}
                )

                if not results:
                    logger.debug("No new jobs found, sleeping")
                    time.sleep(1)
                    continue

                logger.debug("%d new jobs found", len(results))
                for result in results:
                    queue.put(result)
            except Exception as error:
                logger.error('Error while fetching new jobs: %s', error)
                raise

    def _mailing_worker(self, _id: int, queue: multiprocessing.Queue):
        logger = get_colored_logger(f'Sender.mail_worker-{_id}', self._config['log_level'])

        db = get_db(config=self._db_config)

        while True:
            task = queue.get()

            msg_body = f"URL: {task['url']}\nMD5: {task['md5']}"

            if not self._send_email(msg_body=msg_body, reciever=task['email']):
                logger.warning("Couldn't send message to %s", task['email'])
                continue

            db.queries.update_one(
                {'_id': task['_id']},
                {'$set': {'sent': True}}
            )

            logger.warning("Message to %s sent successfully", task['email'])

    def _send_email(self, msg_body: str, reciever: str) -> bool:
        """Send email via SMTP."""
        msg_from = f"From: From {self._config['sender_name']} <{self._config['sender_email']}>"
        msg_to = f"To: To client <{reciever}>"
        msg_subj = f"Subject: [{self._config['service']}] Your hash is calculated!"

        message = f"{msg_from}\n{msg_to}\n{msg_subj}\n\n{msg_body}\n"

        try:
            smtp = smtplib.SMTP_SSL(self._config["host"], self._config["port"])
            self._logger.debug(smtp.ehlo())
            smtp.login(self._config['sender_email'], self._config['sender_pass'])
            smtp.sendmail(self._config['sender_email'], [reciever], message)
            self._logger.info('Message to %s sent successfully', reciever)
            return True
        except smtplib.SMTPException as error:
            self._logger.error('Could not send message to %s: %s', reciever, error)
            return False


def run():
    """Run SMTP sender."""
    proc = Sender('config.json')
    proc.run()


if __name__ == '__main__':
    run()
