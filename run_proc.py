"""Run MD5 processor."""
from md5task import processor


def run():
    """Run MD5 processor."""
    processor.run()


if __name__ == '__main__':
    run()
