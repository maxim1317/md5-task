"""Run flask server."""
from md5task import api


def run():
    """Run flask server."""
    api.run()


if __name__ == '__main__':
    run()
