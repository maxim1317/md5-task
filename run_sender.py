"""Run SMTP sender."""
from md5task import sender


def run():
    """Run SMTP sender."""
    sender.run()


if __name__ == '__main__':
    run()
