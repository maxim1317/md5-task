# MD5 Task

Небольшой Flask сервис для подсчёта MD5 от файла.

## Зависимости

* docker    
* docker-compose

## Подготовка к запуску

Перед запуском необходимо вставить пароль в `config.json` в поле `mail.sender_pass`. Пароль я отправлю в письме по завершению задачи.

## Запуск

```bash
    docker-compose up
```

# Архитектура

```mermaid
graph LR;
  subgraph API
    id1_1["submit/"]
    id1_2["check/"]
  end
  id1_1-->|Создание задачи|id2[MongoDB]
  id2-->|Проверка статуса|id1_2;
  id2-->|Новые задачи|id3_1
  subgraph Processor
    id3_1[queue_worker]-->|Очередь задач|id3_2[processor_worker]
  end
  id5[Target URL]-->|Файл|id3_2
  id3_2-->|Результат работы|id2
  id2-->|Задачи на отправку|id4_1
  subgraph Sender
    id4_1[queue_worker]-->|Очередь|id4_2[sender_worker]
  end
  id4_2-->|Письмо с результатом|id6[smtp.mail.ru]
  id6-->id7[Target email]
  id4_2-->|Результат отправки|id2
```

Решение состоит из четырёх сервисов:

* api
* processor
* sender
* mongo

Сервисы api, processor и sender используют один образ в целях экономии ресурсов.

## API

Простой Flask API, принимает запросы на создание и проверку задач и после простой проверки на корректность данных отправляет задачи в базу.

## Processor

Состоит из нескольких процессов.

1. Отдельный процесс мониторит базу. При появлении новых задач добавляет их в очередь обработки. 
2. Также есть несколько рабочих (число настраивается в `config.json`), которые частями выкачивают файл и, по мере выкачивания, считают MD5. По результатам обновляют записи в базе.

## Sender

Так же, как и processor cостоит из нескольких процессов и устроен схожим образом.

1. Отдельный процесс мониторит базу. При появлении новых задач на отправку добавляет их в очередь отправки.
2. Также есть несколько рабочих (число настраивается в `config.json`), которые формируют и отправляют по SMTP сообщения.

В качестве SMTP сервера я использую `smtp.mail.ru` (gmail.com не дал мне создать аккаунт с моим номером, ya.ru заблокировал рассылку по подозрению в рассылке спама)

## Mongo

Особого смысла в выбор базы я не вкладывал, но решил не экспериментировать и взять то, что ближе.


# Описание API

Ниже представленно описание API.

## Создание задачи

### Запрос

`POST /submit/`

```bash
curl -i -X POST -d "url=http://example.com" http://localhost:8000/submit
```


### Ответ

```
HTTP/1.0 201 CREATED
Content-Type: application/json
Content-Length: 51
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:10:09 GMT
```

```json
{"id": "55c9624e-f80b-11e9-bf49-0242ac130003"}
```


## Создание задачи c указанием поля email

### Запрос

`POST /submit/`

```bash
curl -i -X POST -d "url=http://example.com&email=example@example.com" http://localhost:8000/submit
```


### Ответ

```
HTTP/1.0 201 CREATED
Content-Type: application/json
Content-Length: 51
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:10:09 GMT
```

```json
{"id": "55c9624e-f80b-11e9-bf49-0242ac130003"}
```


### Письмо

```python
---------- MESSAGE FOLLOWS ----------
b'From: From MD5-service <md5task@mail.ru>'
b'To: To client <example@example.com>'
b'[MD5-service] Your hash is calculated!'
b'X-Peer: 172.18.0.4'
b''
b'URL: http://example.com'
b'MD5: 2f282b84e7e608d5852449ed940bfc51'
------------ END MESSAGE ------------
```


## Попытка создания задачи без указания URL

### Запрос

`POST /submit/`

```bash
curl -i -X POST http://localhost:8000/submit
```
 

### Ответ

```bash
HTTP/1.0 400 BAD REQUEST
Content-Type: text/html
Content-Length: 136
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:29:05 GMT
```

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>400 Bad Request</title>
<h1>Bad Request</h1>
<p>You must provide url</p>
```


## Попытка создания задачи c неверным URL

### Запрос

`POST /submit/`

```bash
curl -i -X POST -d "url=example.com" http://localhost:8000/submit
```


### Ответ

```bash
HTTP/1.0 400 BAD REQUEST
Content-Type: text/html
Content-Length: 132
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:36:21 GMT
```

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>400 Bad Request</title>
<h1>Bad Request</h1>
<p>URL is not valid</p>
```


## Попытка создания задачи c неверным email

### Запрос

`POST /submit/`

```bash
curl -i -X POST -d "url=http://example.com&email=example.com" http://localhost:8000/submit
```
  

### Ответ

```bash
HTTP/1.0 400 BAD REQUEST
Content-Type: text/html
Content-Length: 134
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:38:18 GMT
```

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>400 Bad Request</title>
<h1>Bad Request</h1>
<p>Email is not valid</p>
```


## Запрос статуса задачи

### Запрос

`GET /check/`

```bash
curl -i http://localhost:8000/check\?id\=55c9624e-f80b-11e9-bf49-0242ac130003
```


### Ответ

```bash
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 101
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:15:11 GMT
```

```json
{
  "md5": "84238dfc8092e5d9c0dac8ef93371a07", 
  "status": "done", 
  "url": "http://example.com"
}
```


## Запрос статуса задачи, находящейся в процессе выполнения

### Запрос

`GET /check/`

```bash
curl -i http://localhost:8000/check\?id\=55c9624e-f80b-11e9-bf49-0242ac130003
```


### Ответ

```bash
HTTP/1.0 202 ACCEPTED
Content-Type: application/json
Content-Length: 30
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:46:04 GMT
```

```json
{"status": "running"}
```


## Попытка запроса статуса несуществующей задачи

### Запрос

`GET /check/`

```bash
curl -i http://localhost:8000/check\?id\=5555
```


### Ответ

```bash
HTTP/1.0 404 NOT FOUND
Content-Type: application/json
Content-Length: 33
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:48:21 GMT
```

```json
{"status": "not found"}
```


## Запрос статуса проваленной задачи

### Запрос

`GET /check/`

```bash
curl -i http://localhost:8000/check\?id\=55c9624e-f80b-11e9-bf49-0242ac130003
```


### Ответ

```bash
HTTP/1.0 500 INTERNAL SERVER ERROR
Content-Type: application/json
Content-Length: 83
Server: Werkzeug/0.15.4 Python/3.7.4
Date: Sat, 26 Oct 2019 16:51:43 GMT
```

```json
{
  "description": "Error connecting to https://sdkksadkk.com", 
  "status": "failed"
}
```
