FROM python:3.7

WORKDIR /home
COPY md5task/requirements.txt .

RUN pip3 install -r requirements.txt

COPY run_api.py .
COPY run_proc.py .
COPY run_sender.py .

COPY md5task md5task
